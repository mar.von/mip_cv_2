# MIP_cv2

## Popis řešení
V hlavním vlákně  se inicializuje a ovládá displej.
Ve 2. vlákně tiká bomba.
Jakmile se v hlavním vlákně přijde na to, že hodnota minut je menší než 0, bomba vybuchne a přestane se kopat do psa, který po 3s zresetuje zařízení.

## Šablona
Jako šablonu jsem použil template "DISCO-F746NG_LCDTS_demo": [odkaz](https://os.mbed.com/teams/ST/code/DISCO-F746NG_LCDTS_demo//file/9f66aabe7b3b/main.cpp/)

## Zadání
Implementujte jednoduchou minutku (bombu). Po resetu zařízení se na displeji objeví odpočet 2:00. Zařízení odpočítává čas a když dojde k nule, mění displej (“exploze”, “zvonění”) a následně se zařízení resetuje.
Hodnotu času lze upravit pomocí tlačítek s funkcí “plus” a “minus” - dotykem hodnotu časovače snižujeme nebo zvyšujeme o vámi zvolený interval. Kdo si chce hrát, může udělat user-friendly ovládání se samostatnými tlačítky například pro minuty a vteřiny.

Můžete doplnit ještě tlačítko s funkcí “set” pro přepnutí z režimu odpočtu do režimu změny hodnoty časovače. Můžete ale budík udělat i bez této funkce, jde to. (S tím “set” je to zřejmě jednodušší).

Při zpracování úlohy maximálně využijte vše, co už máte v API systému mbed.com hotovo: vlákna, synchronizační nástroje, watchdog atd. atd.

## Co je cílem
Cílem je ukázat, jak snadný je vývoj aplikace, když se opřu byť o jednoduchý operační systém. Cílem je naučit vás správě pracovat s vlákny v mbed a dalšími součástmi.
