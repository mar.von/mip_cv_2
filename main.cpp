#include "mbed.h"
#include "rtos.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"

#define DEFAULT_MINUTES 2
#define DEFAULT_SECONDS 0
#define BUTTON_WIDTH 75
#define BUTTON_EDGE_OFFSET 50
#define BOOM_TIMESPAN 4000
#define WATCHDOG_TIMEOUT 3000


int minutes, seconds;
TS_StateTypeDef TS_State;
Thread t;


void drawButtons()
{
    BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
    int yPos =(BSP_LCD_GetYSize()/2)- (BUTTON_WIDTH/2);
    int xPos =BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_WIDTH);
    
    BSP_LCD_SetTextColor(LCD_COLOR_RED);
    xPos =(BSP_LCD_GetXSize() - BUTTON_EDGE_OFFSET - BUTTON_WIDTH);
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_WIDTH);
    
    
}

void changeTime(int sec)
{
    seconds = seconds + sec;
    if(seconds >= 60)
    {
        minutes++;
        seconds = seconds -60;
    }
    else if(seconds < 0)
    {
       minutes--;
       seconds = 60 + seconds; 
    }
}

void touch()
{
    uint8_t text[30];
    int x, y;

    BSP_TS_GetState(&TS_State);
    if(TS_State.touchDetected) 
    {
        x = TS_State.touchX[0];
        y = TS_State.touchY[0];    
        if(x >= BUTTON_EDGE_OFFSET && x <= BUTTON_EDGE_OFFSET + BUTTON_WIDTH && y >= BSP_LCD_GetYSize()/2 - (BUTTON_WIDTH/2) && y <= BSP_LCD_GetYSize()/2 + (BUTTON_WIDTH/2))
        {
            changeTime(10);
        }
        else if(y >= (BSP_LCD_GetYSize()/2) - (BUTTON_WIDTH/2) && y <= BSP_LCD_GetYSize()/2 + (BUTTON_WIDTH/2) && x >= BSP_LCD_GetXSize() - BUTTON_WIDTH - BUTTON_EDGE_OFFSET && x <= BSP_LCD_GetXSize() - BUTTON_EDGE_OFFSET)
        {
            changeTime(-10);
        } 
        HAL_Delay(200);
    }
    
}

void bombTicking()
{
    while(1)
    {
        seconds--;
        if(seconds < 0)
        {
            seconds = 59;
            minutes--;
        }
        ThisThread::sleep_for(1s);
    }      
    
}

int main() 
{   
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
    
    uint8_t status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    
    if (status != TS_OK) {
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetBackColor(LCD_COLOR_RED);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
    } else {
        BSP_LCD_Clear(LCD_COLOR_GREEN);
        BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
    }
    HAL_Delay(1000);
    
    Watchdog &watchdog = Watchdog::get_instance();
    watchdog.start(WATCHDOG_TIMEOUT);
    
    uint8_t text[30];
    minutes = DEFAULT_MINUTES;
    seconds = DEFAULT_SECONDS; 
    bool reset = false;
    BSP_LCD_SetFont(&Font24);
    BSP_LCD_Clear(LCD_COLOR_GRAY);
    drawButtons();
    BSP_LCD_SetBackColor(LCD_COLOR_GRAY);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    
    t.start(bombTicking);        
    while(!reset)
    {
        sprintf((char*)text, "%d : %2d", minutes, seconds);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)&text, CENTER_MODE);
        touch();
        
        if(minutes <0)
        {
            BSP_LCD_Clear(LCD_COLOR_RED);
            BSP_LCD_SetBackColor(LCD_COLOR_RED);
            BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
            sprintf((char*)text, "BOOM");
            BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)&text, CENTER_MODE);
            HAL_Delay(BOOM_TIMESPAN);
            reset = true;
        }
        watchdog.kick();                     
    }

      
}
